# dyna-imaging-mod
## Article
This code is related to the publication (to be cited if using the code):

`A proof-of-concept pipeline to guide evaluation of tumor tissue perfusion by dynamic contrast-agent imaging: direct simulation and inverse tracer-kinetic procedures`
Authors: Irene E Vignon-Clementel, Nick Jagiella, Jules Dichamp, Jérôme Trowalski, Wiltrud Lederle, Hendrik Laue, Fabian Kiessling, Oliver Sedlaczek, Dirk Drasdo
Original Research, Front. Bioinform. - Computational BioImaging
DOI: 10.3389/fbinf.2023.977228
Contact: irene.vignon-clementel@inria.fr / dirk.drasdo@inria.fr 

## Introduction
This repository contains all necessary code to: generate realistic vascular geometry with or without tumor, solve direct problem of advection (in plasma) - diffusion (in tissue), use this information to solve an inverse problem through Brix2 or Tofts models.
It is composed of the following folders:
* `sources`: contains all code sources: `libnix` (helper library) `Vascularization` (generate geometry direct problem) and `VascularizationInverse` (inverse problem)
* `docker`: contains `Dockerfile` that automatically builds the code in a controled environement.

## Build the code

It is recommended to use the provided `Dockerfile` that will create a Ubuntu 22.04 environment with the necessary dependencies installed.
The `Dockerfile` will also compile all sources and produce executables for each of the code mentioned on previous section.

To build the docker image (from the same directory than this `README.md`):
```terminal
docker build -f ./docker/Dockerfile -t dce-mri .
```

This will create a docker image with tag `dce-mri`. You can check that the image is indeed created by running `docker image ls -a`.

To run the created docker image:
```terminal
docker run -i -t --network host dce-mri
```

The `Dockerfile` will automatically download the latest sources on the [gitlab repository](https://gitlab.inria.fr/simbiotx/dyna-imaging-mod).
The different code executable and libraries will be built in their dedicated folders: `libnix`, `Vascularization`, `VascularizationInverse`.

## Simulation examples
### Direct problem
Inside the docker container:
Create the vascularization

```terminal
mkdir /Output/no_tumor
mkdir /Output/tumor
cd /Output/no_tumor
/build/Vascularization.out -x100 -y100 -z1 -V. -Irandom0 -R100 -X/Output/no_tumor/no_tumor.xml

cd /Output/tumor
/build/Vascularization.out -V/Output/no_tumor/no_tumor.xml -R10 -r7 -Tr15 -Tx25 -Ty25 -Tc10 -N -R100 -X/Output/tumor/tumor.xml
```

Compute the transport in the created vascularization
```terminal
cd /Output/tumor
mkdir conc && cd conc
/build/Vascularization.out -V../tumor.xml -Cupwind
```

### Inverse problem
Inside the docker image

```terminal
cd /Output/tumor/
mkdir inverse && cd inverse
COMMAND="/build/VascularizationInverse.out"
PREF="/Output/tumor/conc/markerMRI00"
SUFF=".C.binary"

for i in $(seq -f "%03g" 0 180)
do
  COMMAND="$COMMAND $PREF$i$SUFF"
done

$COMMAND
```

## Develop in a container

To build the docker development image (from the same directory than this `README.md`):
```terminal
docker build -f ./docker/Dockerfile.dev -t dce-mri_dev .
```
This will create a docker image with tag `dce-mri_dev`. You can check that the image is indeed created by running `docker image ls -a`.

To run the created docker image:
```terminal
docker run -i -t --network host dce-mri_dev --mount type=bind,source="($pwd)"/sources,target=/source
```
You can then develop, compile and run your changes with the required packages.
